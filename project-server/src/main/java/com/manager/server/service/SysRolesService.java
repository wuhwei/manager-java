package com.manager.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.manager.common.pojo.SysRoles;

public interface SysRolesService extends IService<SysRoles> {

    String getUserRoles(String userId);

}
