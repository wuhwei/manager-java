package com.manager.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.manager.common.pojo.User;
import com.manager.common.vo.request.RegisterReq;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
public interface UserService extends IService<User> {

    User getByUsername(String username);


    void register(RegisterReq registerReq);
}
