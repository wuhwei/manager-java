package com.manager.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manager.common.pojo.User;
import com.manager.common.utils.PasswordUtils;
import com.manager.common.vo.request.RegisterReq;
import com.manager.server.mapper.UserMapper;
import com.manager.server.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    /**
     * 根据用户名获取用户
     *
     * @return User
     * @param username
     **/
    @Override
    public User getByUsername(String username) {
        return getOne(new LambdaQueryWrapper<User>().eq(User::getUsername, username));
    }


    @Override
    @Transactional
    public void register(RegisterReq registerReq) {
        User findUserName = getByUsername(registerReq.getUsername());
        Assert.isNull(findUserName, "该用户名已存在!");
        String salt = PasswordUtils.generateSalt();
        String hashPassword = PasswordUtils.hashPassword(registerReq.getPassword(), salt);
        //构建用户信息
        User user =User.builder()
                .username(registerReq.getUsername())
                .nickname(registerReq.getNickname())
                .salt(salt)
                .password(hashPassword)
                .email(registerReq.getEmail())
                .isEnabled(true)
                .build();
        save(user);
    }
}
