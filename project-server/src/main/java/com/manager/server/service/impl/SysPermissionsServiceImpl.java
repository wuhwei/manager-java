package com.manager.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manager.common.pojo.SysPermissions;
import com.manager.server.mapper.SysPermissionsMapper;
import com.manager.server.service.SysPermissionsService;
import org.springframework.stereotype.Service;

@Service
public class SysPermissionsServiceImpl extends ServiceImpl<SysPermissionsMapper, SysPermissions> implements SysPermissionsService {
}
