package com.manager.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manager.common.pojo.Dictionary;
import com.manager.server.mapper.DictionaryMapper;
import com.manager.server.service.DictionaryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
@Service
public class DictionaryServiceImpl extends ServiceImpl<DictionaryMapper, Dictionary> implements DictionaryService {

}
