package com.manager.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manager.common.pojo.UserLog;
import com.manager.server.mapper.UserLogMapper;
import com.manager.server.service.UserLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户操作日志表 服务实现类
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
@Service
public class UserLogServiceImpl extends ServiceImpl<UserLogMapper, UserLog> implements UserLogService {

    @Override
    public void saveUserLog(UserLog userLog) {
        this.save(userLog);
    }

}
