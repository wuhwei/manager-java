package com.manager.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manager.common.pojo.ProjectNode;
import com.manager.server.mapper.ProjectNodeMapper;
import com.manager.server.service.ProjectNodeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目节点信息表 服务实现类
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
@Service
public class ProjectNodeServiceImpl extends ServiceImpl<ProjectNodeMapper, ProjectNode> implements ProjectNodeService {

}
