package com.manager.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manager.common.pojo.SysRoles;
import com.manager.server.mapper.SysRolesMapper;
import com.manager.server.service.SysRolesService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SysRolesServiceImpl extends ServiceImpl<SysRolesMapper, SysRoles> implements SysRolesService {

    /**
     * 获取角色
     *
     * @return String
     **/
    @Override
    public String getUserRoles(String userId) {
        List<SysRoles> roles = baseMapper.selectList(new LambdaQueryWrapper<SysRoles>()
                .inSql(!StringUtils.isEmpty(userId),SysRoles::getRoleId, "select role_id from sys_user_roles where user_id = '" + userId + "'"));
        if (!roles.isEmpty()){
            return roles.stream().map(r -> "ROLE_" + r.getRoleName()).collect(Collectors.joining(","));
        }
        return null;
    }
}
