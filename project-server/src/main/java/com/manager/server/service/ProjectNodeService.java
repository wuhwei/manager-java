package com.manager.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.manager.common.pojo.ProjectNode;

/**
 * <p>
 * 项目节点信息表 服务类
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
public interface ProjectNodeService extends IService<ProjectNode> {

}
