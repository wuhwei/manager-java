package com.manager.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.manager.common.pojo.Dictionary;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
public interface DictionaryService extends IService<Dictionary> {

}
