package com.manager.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.manager.common.pojo.UserLog;

/**
 * <p>
 * 用户操作日志表 服务类
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
public interface UserLogService extends IService<UserLog> {

    void saveUserLog(UserLog userLog);
}
