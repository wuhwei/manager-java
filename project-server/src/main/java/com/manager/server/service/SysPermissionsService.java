package com.manager.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.manager.common.pojo.SysPermissions;

public interface SysPermissionsService extends IService<SysPermissions> {
}
