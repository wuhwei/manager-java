package com.manager.server.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manager.common.pojo.Project;
import com.manager.common.vo.pojoVO.ProjectVO;
import com.manager.common.vo.request.ProjectReq;


/**
 * <p>
 * 项目基本信息表 服务类
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
public interface ProjectService extends IService<Project> {

    IPage<ProjectVO> getProjectList(String name, Integer pageNum, Integer pageSize);

    void addProject(ProjectReq projectReq);
}
