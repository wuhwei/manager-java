package com.manager.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manager.common.pojo.Project;
import com.manager.common.utils.ConvertUtils;
import com.manager.common.vo.pojoVO.ProjectVO;
import com.manager.common.vo.request.ProjectReq;
import com.manager.server.mapper.ProjectMapper;
import com.manager.server.service.ProjectService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目基本信息表 服务实现类
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
@Service
public class ProjectServiceImpl extends ServiceImpl<ProjectMapper, Project> implements ProjectService {

    @Override
    public IPage<ProjectVO> getProjectList(String name, Integer pageNum, Integer pageSize) {
        Page<Project> page = new Page<>(pageNum, pageSize);
        //条件构造
        LambdaQueryWrapper<Project> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StringUtils.isNoneEmpty(name), Project::getProjectName, name)
                .orderByAsc(Project::getProjectWeight);
        //获取列表
        Page<Project> projectPage = baseMapper.selectPage(page, wrapper);
        //构建返回VO
        return projectPage.convert(Project -> ConvertUtils.beanCopy(Project, ProjectVO.class));
    }

    @Override
    public void addProject(ProjectReq projectReq) {
        Project project = ConvertUtils.beanCopy(projectReq, Project.class);
        //project.projectWeight如果为空,则默认为1
        project.setProjectWeight(project.getProjectWeight() == null ? 1 : project.getProjectWeight());
        baseMapper.insert(project);
    }
}
