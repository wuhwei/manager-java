package com.manager.server.security;

import com.manager.common.constent.LogType;
import com.manager.common.constent.RedisKey;
import com.manager.common.utils.JsonUtils;
import com.manager.common.utils.JwtUtils;
import com.manager.common.utils.RedisUtils;
import com.manager.common.vo.dto.LoginUser;
import com.manager.common.vo.response.Result;
import com.manager.server.aop.MyLog;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * 登录成功拦截器
 *
 **/
@Component
public class LoginSuccessHandler implements AuthenticationSuccessHandler {

    private static final String TOKEN = "token";

    @Autowired
    private JwtUtils jwtUtils;

    @MyLog(type = LogType.Login, msg = "用户登录")
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setContentType("application/json;charset=UTF-8");
        ServletOutputStream outputStream = response.getOutputStream();

        // 登录成功后的处理逻辑
        // 1. 获取登录用户信息
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        LoginUser loginUser = (LoginUser) userDetails;
        // 2. 生成JWT token
        String token = jwtUtils.createToken(loginUser.getUsername());
        // 缓存用户信息
        String key = RedisKey.getKey(RedisKey.LoginUser, loginUser.getUsername());
        RedisUtils.set(key, loginUser,JwtUtils.EXPIRE_TIME, TimeUnit.MILLISECONDS);

        // 3. 返回token
        //response.setHeader(TOKEN, token);
        var data = new HashMap<String, Object>();
        data.put(TOKEN, token);

        // 4. 设置响应代码200 CREATED
        response.setStatus(HttpServletResponse.SC_OK);
        outputStream.write(JsonUtils.toStr(Result.success(data)).getBytes(StandardCharsets.UTF_8));
        outputStream.flush();
        outputStream.close();
    }
}


