package com.manager.server.security;

import com.manager.common.constent.RedisKey;
import com.manager.common.utils.JsonUtils;
import com.manager.common.utils.JwtUtils;
import com.manager.common.utils.RedisUtils;
import com.manager.common.vo.dto.LoginUser;
import com.manager.common.vo.response.Result;
import com.manager.server.config.SecurityConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.Assert;
import org.springframework.util.PathMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;


@Slf4j
public class JwtAuthenticationTokenFilter extends BasicAuthenticationFilter {

    @Autowired
    private JwtUtils jwtUtils;

    private final PathMatcher matcher = new AntPathMatcher();


    private static final String AUTHORIZATION = "Authorization";

    public JwtAuthenticationTokenFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        // 判断请求路径是否在白名单中
        String requestPath = request.getRequestURI();
        if (Arrays.stream(SecurityConfig.AUTH_WHITELIST)
                .anyMatch(p -> matcher.match(p, requestPath))) {
            // 请求路径在白名单中，直接放行
            filterChain.doFilter(request, response);
            return;
        }
        try {
            //获取token
            String token = request.getHeader(AUTHORIZATION);
            Assert.notNull(token, "未登录用户!");
            //解析token
            String username = jwtUtils.getUserNameOrNull(token);
            Assert.notNull(username, "token解析失败!");
            // 通过username获取UserDetails，进而创建认证对象
            String key = RedisKey.getKey(RedisKey.LoginUser, username);
            LoginUser loginUser = RedisUtils.get(key, LoginUser.class);
            if (loginUser != null) {
                UsernamePasswordAuthenticationToken authenticationToken =
                        new UsernamePasswordAuthenticationToken(loginUser, null, loginUser.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED); // 设置状态码为401
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(JsonUtils.toStr(
                    Result.fail(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage())
            ).getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
            outputStream.close();
            throw new RuntimeException(e.getMessage());
        }


        //放行
        filterChain.doFilter(request, response);
    }
}
