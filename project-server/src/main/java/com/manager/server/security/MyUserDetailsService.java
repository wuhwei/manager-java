package com.manager.server.security;

import com.manager.common.pojo.User;
import com.manager.common.vo.dto.LoginUser;
import com.manager.server.service.SysRolesService;
import com.manager.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;


@Component
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Autowired
    private SysRolesService sysRolesService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("用户名不存在！");
        } else if (!user.getIsEnabled()) {
            throw new UsernameNotFoundException("用户已停用！");
        }
        return new LoginUser(user, getAuthorities(user.getId()));
    }

    private String getAuthorities(String userId) {
        String userRoles = sysRolesService.getUserRoles(userId);

        return userRoles;
    }

}
