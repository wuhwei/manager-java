package com.manager.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.manager.common.pojo.User;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
public interface UserMapper extends BaseMapper<User> {

}
