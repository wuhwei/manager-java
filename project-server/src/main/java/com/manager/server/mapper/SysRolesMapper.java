package com.manager.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.manager.common.pojo.SysRoles;

public interface SysRolesMapper extends BaseMapper<SysRoles> {
}
