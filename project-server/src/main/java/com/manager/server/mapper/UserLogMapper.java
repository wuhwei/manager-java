package com.manager.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.manager.common.pojo.UserLog;

/**
 * <p>
 * 用户操作日志表 Mapper 接口
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
public interface UserLogMapper extends BaseMapper<UserLog> {

}
