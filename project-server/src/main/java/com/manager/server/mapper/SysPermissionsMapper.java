package com.manager.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.manager.common.pojo.SysPermissions;

public interface SysPermissionsMapper extends BaseMapper<SysPermissions> {
}
