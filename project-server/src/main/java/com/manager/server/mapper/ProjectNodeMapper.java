package com.manager.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.manager.common.pojo.ProjectNode;

/**
 * <p>
 * 项目节点信息表 Mapper 接口
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
public interface ProjectNodeMapper extends BaseMapper<ProjectNode> {

}
