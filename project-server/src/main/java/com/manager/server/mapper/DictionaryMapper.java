package com.manager.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.manager.common.pojo.Dictionary;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
public interface DictionaryMapper extends BaseMapper<Dictionary> {

}
