package com.manager.server.config;


import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class MybatisPlusAutoFillConfig implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject, "createdTime",LocalDateTime.class, LocalDateTime.now());
        this.strictUpdateFill(metaObject, "updatedTime",LocalDateTime.class, LocalDateTime.now());
        this.strictInsertFill(metaObject, "isDeleted",Boolean.class, false);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.strictUpdateFill(metaObject, "updatedTime",LocalDateTime.class, LocalDateTime.now());
    }
}
