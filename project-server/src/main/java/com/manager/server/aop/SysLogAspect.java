package com.manager.server.aop;

import com.manager.common.constent.LogType;
import com.manager.common.pojo.UserLog;
import com.manager.common.utils.IpUtils;
import com.manager.common.utils.JsonUtils;
import com.manager.common.utils.JwtUtils;
import com.manager.server.service.UserLogService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * @ClassName LoginLogAspect
 * @Description  操作日志切入面
 *
 **/
@Aspect
@Slf4j
@Component
public class SysLogAspect {

    @Autowired
    private UserLogService userLogService;

    @Autowired
    private JwtUtils util;

    //定义切点 @Pointcut
    //在注解的位置切入代码
    @Pointcut("@annotation( com.manager.server.aop.MyLog)")
    public void logPoinCut() {

    }

    //切面 配置通知
    @AfterReturning("logPoinCut()")
    public void savaLog(JoinPoint joinpoin){
        UserLog userLog = new UserLog();

        //从切面织入点处通过反射机制获取织入点处的方法
        MethodSignature signature = (MethodSignature) joinpoin.getSignature();
        //获取切入点所在的方法
        Method method = signature.getMethod();
        MyLog myLog = method.getAnnotation(MyLog.class);
        if(myLog != null){
            String type  = myLog.type();
            userLog.setAction(type);
        }
        //获取请求的类名
        //String className = joinpoin.getTarget().getClass().getName();

        //获取请求的方法名
        String methodName = method.getName();
        HttpServletRequest request=((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        if (LogType.Login.equals(myLog.type())){
            Object[] args = joinpoin.getArgs();
            for (Object arg : args) {
                if (arg instanceof UsernamePasswordAuthenticationToken) {
                    UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) arg;
                    UserDetails userDetails = (UserDetails) auth.getPrincipal();
                    String username = userDetails.getUsername();
                    userLog.setUserId(username);
                    userLog.setMsg(myLog.msg());
                }
            }
        }else {
            String token  = request.getHeader("Authorization");
            String username = util.getUserNameOrNull(token);
            userLog.setUserId(username);
            //请求参数
            Object[] args = joinpoin.getArgs();
            String param = JsonUtils.toStr(args);
            userLog.setMsg(methodName+":"+param);
        }

        String ip = IpUtils.getRealIp(request);
        userLog.setIpAddress(ip);

        userLogService.save(userLog);
    }

}