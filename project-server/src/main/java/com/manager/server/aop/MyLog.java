package com.manager.server.aop;

import java.lang.annotation.*;

@Target({ ElementType.METHOD})    //方法上
@Retention(RetentionPolicy.RUNTIME)
@Documented
public  @interface MyLog {

    /** 操作类型 **/
    String type() default "";

    /** 操作解释 **/
    String msg() default "";
}
