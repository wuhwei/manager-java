package com.manager.server.controller;


import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.manager.common.vo.request.LoginReq;
import com.manager.common.vo.response.Result;
import com.manager.server.service.UserLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * <p>
 * 用户操作日志表 前端控制器
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
@RestController
@RequestMapping("/userlog")
@Api(tags = "Api-用户日志信息控制器")
@ApiSort(5)
public class UserLogController {

    @Autowired
    private UserLogService userLogService;

    @ApiOperation(value = "api-查询日志", position = 1)
    @PostMapping("/getLog")
    public Result<Void> login(@Valid @RequestBody LoginReq loginReq) {
        return Result.success();
    }


}

