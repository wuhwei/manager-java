package com.manager.server.controller;

import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.manager.common.utils.JsonUtils;
import com.manager.common.utils.MinioClientUtil;
import com.manager.common.vo.response.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author yujie.li
 * @description
 * @date 2023/7/18
 */
@RestController
@Slf4j
@Api(tags = "Api-Minio")
@ApiSort(9)
public class MinioController {
    @Autowired
    private MinioClientUtil minioUtil;

    @ApiOperation(value = "api-上传", position = 1)
    @PostMapping("/upload")
    public Result<String> upload(@RequestPart MultipartFile file) {
        String filePath;
        try {
            filePath = minioUtil.putObject(file);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail("上传失败");
        }
        return Result.success(filePath);
    }

    @ApiOperation(value = "api-下载", position = 2)
    @GetMapping("/download")
    public void download(HttpServletResponse response, @RequestParam(value = "filepath") String filepath) throws IOException {
        try {
            minioUtil.getObject(response, filepath);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("下载失败", e);
            response.reset();
            response.setContentType("application/json;charset=UTF-8");
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(JsonUtils.toStr(Result.fail(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage())).getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
            outputStream.close();
        }
    }
}