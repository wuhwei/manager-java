package com.manager.server.controller;


import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.manager.common.vo.request.LoginReq;
import com.manager.common.vo.request.RegisterReq;
import com.manager.common.vo.response.Result;
import com.manager.server.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@Api(tags = "Api-登录注册控制器")
@ApiSort(1)
public class LoginRegisterController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "api-登录接口", position = 1)
    //@ApiOperationSupport(order = 1)
    @PostMapping("/login")
    public Result<Void> login(@Valid @RequestBody LoginReq loginReq) {
        return Result.success();
    }

    @ApiOperation(value = "api-注册接口", position = 2)
    //@ApiOperationSupport(order = 2)
    @PostMapping("/register")
    public Result<Void> register(@Valid @RequestBody RegisterReq registerReq) {
        userService.register(registerReq);
        return Result.success();
    }

}
