package com.manager.server.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.manager.common.constent.LogType;
import com.manager.common.vo.pojoVO.ProjectVO;
import com.manager.common.vo.request.ProjectReq;
import com.manager.common.vo.response.Result;
import com.manager.server.aop.MyLog;
import com.manager.server.service.ProjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 项目基本信息表 前端控制器
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
@RestController
@RequestMapping("/project")
@Api(tags = "Api-项目信息控制器")
@ApiSort(2)
public class ProjectController {


    @Autowired
    private ProjectService projectService;

    @ApiOperation(value = "api-获取项目列表", position = 1)
    @MyLog(type = LogType.SELECT, msg = "获取项目列表")
    @Cacheable(value = "projectCache",key = "#pageNum+'_'+#pageSize",condition = "#name == null",unless = "#result == null")
    @GetMapping("/getProject")
    public Result<IPage<ProjectVO>> getProjectList(
            @RequestParam(name = "name",required = false) String name,
            @RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
            @RequestParam(name = "pageSize", defaultValue = "8") Integer pageSize) {
        return Result.success(projectService.getProjectList(name, pageNum, pageSize));
    }

    @ApiOperation(value = "api-添加项目", position = 2)
    @MyLog(type = LogType.INSERT, msg = "新增项目信息")
    @PreAuthorize("hasAnyRole('admin')")
    @CacheEvict(value = "projectCache",allEntries = true)
    @PostMapping("/addProject")
    public Result<Void> addProject(@RequestBody @Valid ProjectReq projectReq) {
        projectService.addProject(projectReq);
        return Result.success();
    }

}

