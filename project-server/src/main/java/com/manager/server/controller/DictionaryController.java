package com.manager.server.controller;


import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 字典表 前端控制器
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
@RestController
@RequestMapping("/dictionary")
@Api(tags = "Api-字典信息控制器")
@ApiSort(6)
public class DictionaryController {

}

