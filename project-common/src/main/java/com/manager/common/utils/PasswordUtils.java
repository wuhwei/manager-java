package com.manager.common.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

public class PasswordUtils {
    private static final int SALT_LENGTH = 16; // 盐值长度，单位为字节
    private static final String HASH_ALGORITHM = "SHA-256"; // 哈希算法

    // 生成盐值
    public static String generateSalt() {
        byte[] salt = new byte[SALT_LENGTH];
        new SecureRandom().nextBytes(salt);
        return Base64.getEncoder().encodeToString(salt);
    }

    // 对密码进行加盐哈希处理
    public static String hashPassword(String password, String salt) {
        String input = password + salt;
        try {
            MessageDigest digest = MessageDigest.getInstance(HASH_ALGORITHM);
            byte[] hash = digest.digest(input.getBytes());
            return Base64.getEncoder().encodeToString(hash);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Unable to hash password: " + e.getMessage(), e);
        }
    }

    // 验证密码是否匹配
    public static boolean verifyPassword(String inputPassword, String salt, String hashedPassword) {
        String hashedInput = hashPassword(inputPassword, salt);
        return hashedInput.equals(hashedPassword);
    }

    // 测试
//    public static void main(String[] args) {
//        // 模拟用户注册场景
//        String password = "admin123"; // 用户输入的原始密码
//        String salt = generateSalt(); // 生成盐值
//        String hashedPassword = hashPassword(password, salt); // 加盐哈希处理后的密码
//
//        System.out.println("Salt: " + salt);
//        System.out.println("Hashed Password: " + hashedPassword);
//
//        // 模拟用户登录场景
//        String inputPassword = "admin"; // 用户输入的登录密码
//        boolean isMatch = verifyPassword(inputPassword, salt, hashedPassword); // 验证密码是否匹配
//        System.out.println("Password Match: " + isMatch);
//    }
}
