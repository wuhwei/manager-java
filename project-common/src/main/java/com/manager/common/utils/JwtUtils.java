package com.manager.common.utils;

import cn.hutool.core.util.StrUtil;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;
import java.util.Optional;

/**
 * Description: jwt的token生成与解析
 *
 *
 */
@Slf4j
@Component
public class JwtUtils {

    /**
     * token秘钥，请勿泄露，请勿随便修改
     */
    @Value("${jwt.secret}:manager")
    private String secret = "manager";

    private static final String USERNAME_CLAIM = "username";
    private static final String CREATE_TIME = "createTime";

    public static final long EXPIRE_TIME = 1000 * 60 * 30; //30分钟

    /**
     * JWT生成Token.<br/>
     * <p>
     * JWT构成: header, payload, signature
     */
    public String createToken(String username) {
        // build token
        String token = JWT.create()
                .withClaim(USERNAME_CLAIM, username) // 只存一个uid信息，其他的自己去redis查
                .withClaim(CREATE_TIME, new Date())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRE_TIME))
                .sign(Algorithm.HMAC256(secret)); // signature
        return token;
    }


    /**
     * 解密Token
     *
     * @param token
     * @return
     */
    public Map<String, Claim> verifyToken(String token) {
        if (StrUtil.isEmpty(token)) {
            return null;
        }
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(secret)).build();
            DecodedJWT jwt = verifier.verify(token);
            return jwt.getClaims();
        } catch (Exception e) {
            log.info("decode error,token:{}", token, e);
        }
        return null;
    }


    /**
     * 根据Token获取uid
     *
     * @param token
     * @return uid
     */
    public String getUserNameOrNull(String token) {
        return Optional.ofNullable(verifyToken(token))
                .map(map -> map.get(USERNAME_CLAIM))
                .map(Claim::asString)
                .orElse(null);
    }


    public static void main(String[] args) {
        JwtUtils utils = new JwtUtils();

        String userNameOrNull = utils.getUserNameOrNull("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVUaW1lIjoxNjkxNDc1MDg3LCJleHAiOjE2OTE0NzUwODksInVzZXJuYW1lIjoiYWRtaW4ifQ.ThgubdoU0ril4v2vGN73j8-xk0hQIjPKq_l-PDtZRCk");

        System.out.println("userNameOrNull = " + userNameOrNull);

    }

}
