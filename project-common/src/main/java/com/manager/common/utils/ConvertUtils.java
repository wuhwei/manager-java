package com.manager.common.utils;

import org.springframework.beans.BeanUtils;

public class ConvertUtils {

    public static <T> T beanCopy(Object source, Class<T> targetClass) {
        try {
            T targetInstance = targetClass.getDeclaredConstructor().newInstance();
            BeanUtils.copyProperties(source, targetInstance);
            return targetInstance;
        } catch (Exception e) {
            throw new RuntimeException("属性转换错误！", e);
        }
    }

}
