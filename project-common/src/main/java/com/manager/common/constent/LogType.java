package com.manager.common.constent;

public class LogType {

    public static final String Login = "登录";
    public static final String UPDATE = "修改";
    public static final String DELETE = "删除";
    public static final String SELECT = "查询";
    public static final String INSERT = "新增";
}
