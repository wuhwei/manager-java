package com.manager.common.constent;

public class RedisKey {

    private static final String BASE_KEY = "manager:";

    public static final String LoginUser = "loginUser:username_%s";


    public static String getKey(String key, Object... objects) {
        return BASE_KEY + String.format(key, objects);
    }


}
