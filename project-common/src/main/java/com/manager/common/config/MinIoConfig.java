package com.manager.common.config;

import io.minio.MinioClient;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: wuhwei
 * @description
 * @date 2023/7/18
 */
@Data
@Configuration
/* 加载yml文件中以minio开头的配置项 */
@ConfigurationProperties(prefix = "minio")
public class MinIoConfig {


    /* 会自动的对应配置项中对应的key*/
    private String endpoint;//minio.endpoint
    private String accessKey;
    private String secretKey;
    private String bucketName;

    /**
     * 创建MinIo客户端
     *
     */
    @Bean
    public MinioClient minioClient() {

        return MinioClient.builder()
                .endpoint(endpoint)
                .credentials(accessKey, secretKey)
                .build();
    }

}