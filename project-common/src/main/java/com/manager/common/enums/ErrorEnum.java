package com.manager.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ErrorEnum {

    ACCESS_DENIED(401, "未登录的用户"),

    LOGIN_DENIED(403 , "用户名或密码错误"),

    PARAM_VALID(-2, "参数校验失败"),

    FAIL(1, "操作失败"),
    ERR(500, "请求失败");

    private final Integer code;
    private final String msg;
}
