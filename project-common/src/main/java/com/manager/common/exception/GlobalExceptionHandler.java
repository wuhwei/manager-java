package com.manager.common.exception;


import com.manager.common.enums.ErrorEnum;
import com.manager.common.vo.response.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
@Component
public class GlobalExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    public Result<Void> RuntimeException(RuntimeException e){
        log.error("RuntimeException:------->{}",e.getMessage());
        return Result.fail(e.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result<Void> methodArgumentNotValidException(MethodArgumentNotValidException e){
        StringBuilder errorMsg = new StringBuilder();
        e.getBindingResult().getFieldErrors().forEach(x -> errorMsg.append(x.getField()).append(x.getDefaultMessage()).append(","));
        String message = errorMsg.toString();
        log.info("validation parameters error！The reason is:{}", message);
        return Result.fail(ErrorEnum.PARAM_VALID.getCode(),  message.substring(0, message.length() - 1));
    }


    @ExceptionHandler(UsernameNotFoundException.class)
    public Result<Void> usernameNotFoundException(UsernameNotFoundException e){
        log.error("UsernameNotFoundException:------->{}",e.getMessage());
        return Result.fail(e.getMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public Result<Void> illegalArgumentException(IllegalArgumentException e){
        log.error("illegalArgumentException:------->{}",e.getMessage());
        return Result.fail(e.getMessage());
    }

}
