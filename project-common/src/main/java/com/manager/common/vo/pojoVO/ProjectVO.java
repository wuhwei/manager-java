package com.manager.common.vo.pojoVO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * <p>
 * 项目基本信息表
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("项目信息VO")
public class ProjectVO {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("项目名称")
    private String projectName;

    @ApiModelProperty("项目图片")
    private String projectImage;

    @ApiModelProperty("项目简介")
    private String projectDescription;

    @ApiModelProperty("项目状态")
    private String projectStatus;

    @ApiModelProperty("创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    @ApiModelProperty("更新时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

    @ApiModelProperty("项目链接")
    private String projectLink;


}
