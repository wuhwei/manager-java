package com.manager.common.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("项目信息请求体")
public class ProjectReq {

    @ApiModelProperty("项目名称")
    @NotNull
    @NotBlank
    private String projectName;

    @ApiModelProperty("项目图片")
    @URL
    private String projectImage;

    @ApiModelProperty("项目简介")
    private String projectDescription;

    @ApiModelProperty("项目状态")
    @NotNull
    @NotBlank
    private String projectStatus;

    @ApiModelProperty("项目权重")
    @Min(value = 1, message = "权重必须大于0")
    @Max(value = 9, message = "权重最大为9")
    private Integer projectWeight;


    @ApiModelProperty("项目链接")
    private String projectLink;


}