package com.manager.common.vo.request;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "注册请求")
public class RegisterReq {

    @ApiModelProperty(value = "用户名")
    @NotBlank
    @Pattern(regexp = "^\\w{3,20}$\n")  //数字字母下划线，3~20位
    private String username;

    @ApiModelProperty(value = "密码")
    @NotBlank
    @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$\n") //密码大于6位，数字字母下划线
    private String password;

    @ApiModelProperty(value = "昵称")
    @NotBlank
    @Length(max = 20)
    private String nickname;

    @ApiModelProperty(value = "邮箱")
    @Email
    private String email;
}
