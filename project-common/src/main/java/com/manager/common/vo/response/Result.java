package com.manager.common.vo.response;


import com.manager.common.enums.ErrorEnum;
import lombok.Data;

/**
 * 通用返回体
 **/

@Data
public class Result<T> {
    private Boolean success;

    private Integer errCode;

    private String errMsg;

    private T data;

    public static <T> Result<T> success() {
        Result<T> result = new Result<T>();
        result.setData(null);
        result.setSuccess(Boolean.TRUE);
        return result;
    }

    public static <T> Result<T> success(T data) {
        Result<T> result = new Result<T>();
        result.setData(data);
        result.setSuccess(Boolean.TRUE);
        return result;
    }

    public static <T> Result<T> fail(Integer code, String msg) {
        Result<T> result = new Result<T>();
        result.setSuccess(Boolean.FALSE);
        result.setErrCode(code);
        result.setErrMsg(msg);
        return result;
    }

    public static <T> Result<T> fail(String msg) {
        Result<T> result = new Result<T>();
        result.setSuccess(Boolean.FALSE);
        result.setErrCode(ErrorEnum.ERR.getCode());
        result.setErrMsg(msg);
        return result;
    }

    public static <T> Result<T> fail(ErrorEnum errorEnum) {
        Result<T> result = new Result<T>();
        result.setSuccess(Boolean.FALSE);
        result.setErrCode(errorEnum.getCode());
        result.setErrMsg(errorEnum.getMsg());
        return result;
    }

    public boolean isSuccess() {
        return this.success;
    }

}
