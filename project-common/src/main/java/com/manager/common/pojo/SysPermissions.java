package com.manager.common.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel("权限信息表")
public class SysPermissions implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty(value = "id")
    @TableId(value = "permission_id", type = IdType.ASSIGN_UUID)
    private String permissionId;

    /**
     * 角色名称
     */
    @ApiModelProperty(value = "权限名称")
    private String permissionName;

    /**
     * 角色描述
     */
    @ApiModelProperty(value = "权限描述")
    private String description;

}
