package com.manager.common.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

/**
 * <p>
 * 用户信息表
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel("用户信息表")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户的唯一标识符
     */
    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 用户名 (不允许为空)
     */
    @ApiModelProperty(value = "用户名")
    private String username;

    /**
     * 用户密码 (不允许为空)
     */
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * 密码盐值，用于加密密码
     */
    @ApiModelProperty(value = "密码盐值")
    private String salt;

    /**
     * 用户昵称
     */
    @ApiModelProperty(value = "用户昵称")
    private String nickname;

    /**
     * 用户邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;


    /**
     * 标记用户是否启用
     */
    @ApiModelProperty(value = "是否启用")
    private Boolean isEnabled;


}
