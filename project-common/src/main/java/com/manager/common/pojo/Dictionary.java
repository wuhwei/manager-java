package com.manager.common.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 字典表
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("字典表信息")
public class Dictionary implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字典项的唯一标识符
     */
    @ApiModelProperty("id")
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 字典项分类编码 (不允许为空)
     */
    @ApiModelProperty("分类编码")
    private String itemType;

    /**
     * 字典分类描述 (不允许为空)
     */
    @ApiModelProperty("分类描述")
    private String itemDescription;

    /**
     * 字典编码 (不允许为空)
     */
    @ApiModelProperty("编码")
    private String itemCode;

    /**
     * 字典编码描述 (不允许为空)
     */
    @ApiModelProperty("编码描述")
    private String itemDescribe;

    /**
     * 标记字典项是否启用
     */
    @ApiModelProperty("是否启用")
    private Boolean isActive;

    /**
     * 字典项创建时间
     */
    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 字典项更新时间
     */
    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

    /**
     * 标记字典是否被删除（是/否）
     */
    @ApiModelProperty("是否删除")
    @TableField(fill = FieldFill.INSERT)
    private Boolean isDeleted;


}
