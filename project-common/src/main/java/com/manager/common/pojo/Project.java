package com.manager.common.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 项目基本信息表
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("项目基本信息表")
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 项目的唯一标识符
     */
    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 项目的名称 (不允许为空)
     */
    @ApiModelProperty(value = "项目名称")
    private String projectName;

    /**
     * 项目首页图片的存储地址
     */
    @ApiModelProperty(value = "项目首页图片")
    private String projectImage;

    /**
     * 对项目的简要介绍
     */
    @ApiModelProperty(value = "项目简介")
    private String projectDescription;

    /**
     * 项目的状态（例如：进行中、已完成等）
     */
    @ApiModelProperty(value = "项目状态")
    private String projectStatus;

    /**
     * 项目的相关链接（可选）
     */
    @ApiModelProperty(value = "项目相关链接")
    private String projectLink;

    /**
     * 项目权重，用于指示项目排序优先级
     */
    @ApiModelProperty(value = "项目权重，用于指示项目排序优先级")
    private Integer projectWeight;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdTime;

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updatedTime;

    /**
     * 标记项目是否被删除（是/否）
     */
    @ApiModelProperty("是否删除")
    @TableField(fill = FieldFill.INSERT)
    private Boolean isDeleted;


}
