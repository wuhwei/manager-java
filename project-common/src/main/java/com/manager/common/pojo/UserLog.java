package com.manager.common.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户操作日志表
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("用户操作日志表")
public class UserLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 日志的唯一标识符
     */
    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 关联的用户ID
     */
    @ApiModelProperty(value = "用户id")
    private String userId;

    /**
     * 操作名称 (不允许为空)
     */
    @ApiModelProperty(value = "操作名称")
    private String action;

    /**
     * 操作名称 (不允许为空)
     */
    @ApiModelProperty(value = "操作详情")
    private String msg;

    /**
     * 日志记录时间
     */
    @ApiModelProperty(value = "记录时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdTime;

    /**
     * 用户IP地址
     */
    @ApiModelProperty(value = "用户IP地址")
    private String ipAddress;


}
