package com.manager.common.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 项目节点信息表
 * </p>
 *
 * @author Whw
 * @since 2023-07-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ProjectNode implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("id")
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @ApiModelProperty("节点名称")
    private String nodeName;

    @ApiModelProperty("节点状态")
    private String nodeStatus;

    @ApiModelProperty("节点开始时间")
    private Date startTime;


    @ApiModelProperty("节点结束时间")
    private Date endTime;


    @ApiModelProperty("节点权重")
    private Integer nodeWeight;


    @ApiModelProperty("节点相关链接")
    private String link;


    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdTime;


    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updatedTime;


    @ApiModelProperty("是否删除")
    @TableField(fill = FieldFill.INSERT)
    private Boolean isDeleted;


}
